import json
import socket
import sys
import pickle
import os
import random
import convertToCsv
import math
import pdb
import numpy
from numpy import matrix
from numpy import linalg

def getNewFilename():
    num = 1
    while os.path.exists('messages-' + str(num) + '.p'):
        num = num + 1
    return 'messages-' + str(num) + '.p'

def pS(S):
    print "\n".join(map(repr, S))

class State:
    v = 0
    x = 0
    p = 0
    t = 0
    a = 0
    w = 0    
    l = 0
    tick = -1
    lap = 0

    def dist(self, another):
        return (
            abs(self.v - another.v) +
            abs(self.x - another.x) +
            abs(self.p - another.p) +
            abs(self.t - another.t) +
            abs(self.a - another.a) +
            abs(self.w - another.w) +
            abs(self.l - another.l)
        )

    def __repr__(self):
        return "v: " + str(self.v) + " x: " + str(self.x) +" p: " + str(self.p) +" t: " + str(self.t) + " a: " + str(self.a) +" w: " + str(self.w) +" l: " + str(self.l) 
          
class NoobBot(object):
    
    tick = 0
    prevS = State()
    collectingData = True
    collectingTurboData = True
    turbo = {"available":False, "on":False}
    fittedFinished = State()
    bestStartingPoint = State()
    alreadySwitched = False
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def msg(self, msg_type, data, gameTick):
        if gameTick == None:
            sentData = {"msgType": msg_type, "data": data}
        else:
            sentData = {"msgType": msg_type, "data": data, "gameTick": gameTick}
        if msg_type != "ping":
            self.send(json.dumps(sentData))
        return sentData

    def join(self):
        return self.msg("joinRace", {
      "botId": {
        "name": self.name,
        "key": self.key
      },
#      "trackName":"keimola",
#      "trackName":"germany",
#      "trackName":"usa",
      "trackName":"france",
      "carCount": 1
    }, None)
    def throttle(self, throttle, gameTick):
        return self.msg("throttle", throttle, gameTick)

    def ping(self):
        return self.msg("ping", {}, None)

    def on_join(self, data):
        print("Joined")
        return self.ping()

    def on_game_start(self, data):
        print("Race started")
        return self.ping()    
    
    def curvedLength(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return correctedRadius * abs(piece['angle'])/360.0 * 2 * math.pi
    
    def curvature(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return 1.0/correctedRadius

    def on_gameinit(self, data):
        self.track = data["race"]["track"]
        self.track["params"] = { "c1": 0.98, "c2": 0.2, "c3": 0.9, "c4": -0.00125, "c5": -0.3, "c6": 0.28125}
        self.track["params"]["c6.sqrt"] = math.sqrt(self.track["params"]["c6"])
        self.S = []
        self.SS = []
        self.OS = []
        
        for piece in self.track["pieces"]:
            piece["curvatureSign"] = 0
            if not("length" in piece):
                if(piece['angle'] > 0):
                    piece["curvatureSign"] = 1
                else:
                    piece["curvatureSign"] = -1
                piece['length'] = [self.curvedLength(piece, lane) for lane in self.track['lanes']]
                piece['curvature'] = [self.curvature(piece, lane) for lane in self.track['lanes']]
                piece['curvature.sqrt'] = [math.sqrt(c) for c in piece['curvature']]
        print("Game inited, track "+self.track["name"])
        return self.ping()

    def throttleToKeepNextVelocity(self, s):
        pm = self.track["params"]  
        turboFactor = 1.0
        if self.turbo["on"] == True and self.turbo["startTick"] + self.turbo["turboDurationTicks"] >= s.tick:
            turboFactor = self.turbo["turboFactor"]
        return max(0.0,min(1.0, (1-pm["c1"]) * s.v / pm["c2"] / turboFactor))

    def parseThrottle(self, s, t):
        if t == -1:
            return self.throttleToKeepNextVelocity(s)
        else:
            return t

    def simulateCrash(self, s, T, nt0):
        maxAngle = 59.9999999
        for t in T:
            s = self.simulate(s, self.parseThrottle(s,t))
            if abs(s.a) > maxAngle:
                return {"crash": True}
        s = self.simulate(s, 0)
        if abs(s.a) > maxAngle:
            return {"crash": True}
        s = self.simulate(s, 0)
        if abs(s.a) > maxAngle:
            return {"crash": True}
        fs = s
        for i in range(nt0):
            s = self.simulate(s, 0)
            if abs(s.a) > maxAngle:
                return {"crash": True}
        return {"crash": False, "finalState": fs}
    
    def on_lapfinished(self,data):
        print "Lap time: ",data["lapTime"]["millis"]*0.001
        return self.ping()
    def switch_lane(self, slane, gameTick):
        return self.msg("switchLane", slane, gameTick)
   
   
    def append_os(self, data):
        for i in range(0,len(data)):
            if data[i]["id"]["name"] != self.name:
                if not data[i]["id"]["name"] in self.OS:
                    self.OS[data[i]["id"]["name"]] = []
                tempstate = State()
                tempstate-
                self.OS[data[i]["id"]["name"]].append()
                

   
    def on_car_positions(self, data):
        self.tick += 1

#        if self.tick > 1:
#            assert self.tick == data[0]['gameTick'] + 1

        if(self.collectingData): #fit model
            throttle_value = random.uniform(0.99, 1.0)
        else:
            if(self.prevS.v == 0):
                throttle_value = 1.0
            else:
                #BN = 4
                #BL = [1,3,3,3]
                BN = 4
                BL = [1,1,1,2]

                #BL = [5, ] * BN
                #T = [-1,] * sum(BL)
                T = [0,] * sum(BL)

                Bt = -1
                BS = self.prevS

                tl = len(self.track["pieces"]) 

                #M = 3**BN;
                M = 2**BN;
                for k in range(M):
                    kk = k
                    bi = 0
                    for b in range(BN):
                        #t = kk % 3 - 1
                        #kk /= 3
                        t = kk % 2
                        kk /= 2
                        for i in range(BL[b]):
                            T[bi + i] = t
                        bi += BL[b]

                    sc = self.simulateCrash(self.prevS, T, 300)
                    if not sc["crash"]:
                        sc = sc["finalState"]
                        p1 = (BS.p - self.prevS.p + tl) % tl 
                        p2 = (sc.p - self.prevS.p + tl) % tl 
                        if(p2 > p1 or (p2 == p1 and sc.x > BS.x)):
                            BS = sc
                            Bt = T[0]
         
                throttle_value = self.parseThrottle(self.prevS, Bt)

        #if not self.simulateCrash(self.prevS, 1.0):
        #    throttle_value = 1.0
        #else:
        #    tmin = 0.0
        #    tmax = 1.0
        #    while tmax - tmin > 0.001:
        #        pivot = (tmin+tmax)*0.5
        #        if(self.simulateCrash(self.prevS, pivot)):
        #            tmax = pivot
        #        else:
        #            tmin = pivot
        #    throttle_value = tmin            

        #throttle_value = random.uniform(0.6,0.7)
        
        self.SS.append(self.simulate(self.prevS, throttle_value))
        
        if self.tick==4:
            self.prevS = self.proceed(self.prevS, self.prevS.t, data)
        else:
            self.prevS = self.proceed(self.prevS, throttle_value, data)
            
        self.S.append(self.prevS)
        
        if(self.tick % 30 == 0):
            print str(self.tick) + ": " + str(self.S[-1].dist(self.SS[-1]))
        #if not(self.collectingData) and self.S[-1].dist(self.SS[-1]) > 0.35:
        #    pdb.set_trace()

        self.collectingTurboData = False
        self.bestStartingPoint = State()
        self.bestStartingPoint.p =35
        self.bestStartingPoint.x =56.677601965
        
        #if not self.collectingData and self.collectingTurboData:
        #    if (self.fittedFinished.lap + 1 == self.prevS.lap and self.fittedFinished.p + 2 < self.prevS.p) or (self.fittedFinished.lap + 2 <= self.prevS.lap):
        #        self.optimize_turbo()
        
        
        if self.tick == 4:
            return self.switch_lane("Right", self.tick-1)
        elif not self.collectingTurboData and self.turbo["available"] and throttle_value > 0.99 and ((self.prevS.p == self.bestStartingPoint.p and self.prevS.x > self.bestStartingPoint.x) or self.prevS.p == (self.bestStartingPoint.p + 1) % len(self.track["pieces"])):
            return self.turbodo() 
        else:
            return self.throttle(throttle_value, self.tick - 1)

    def on_crash(self, data):
        print("Someone crashed, with prev angle")
        print(self.prevS.a)
        if(self.collectingData):
            self.collectingData = False
            self.solveTrackParams()
        return self.ping()

    def on_turbostart(self, data):
        self.turbo["on"] = True
        self.turbo["startTick"] = self.tick
        self.turbo["available"] = False
        return self.ping()
    
    def on_turboend(self, data):
        self.turbo["on"] = False
        return self.ping()
    
    def turbodo(self):
        self.turbo["available"] = False
        print("started turbo")
        return self.msg("turbo", "wroom wroom", self.tick-1)
 
    def on_turboavailable(self, data):
        if not self.turbo["on"]:
            self.turbo = data
            self.turbo["available"] = True
            self.turbo["on"] = False
        print("Turbo is available")

    def optimize_turbo(self):
        l = -1
        bi = 1
        bl = 0
        ok = False
        for i in range(self.fittedFinished.tick+2, len(self.S)):
            if(self.S[i].t != 1.0):
                ok = True

            if(ok):
                if(self.S[i].t == 1.0):
                    if(l == -1):
                        l=1
                    else:
                        l+=1
                else:
                    if(l > bl):
                        bi = i - l
                        bl = l
                    l = -1
        self.bestStartingPoint = self.S[bi]

        self.collectingTurboData = False

        print(self.S[bi])

        return self.ping()

    def on_game_end(self, data):
        print("Race ended")
        return self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        return self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_gameinit,
            'lapFinished': self.on_lapfinished,
            'turboAvailable': self.on_turboavailable,
            'turboStart': self.on_turbostart,
            'turboEnd': self.on_turboend
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        messages = []
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            #if 'gameTick' in msg and data != None and msg_type == 'carPositions':
            #    data[0]['gameTick'] = msg['gameTick']
            if msg_type in msg_map:
                return_msg = msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                return_msg = self.ping()
            messages.append({'msg': msg, 'return': return_msg})
            line = socket_file.readline()
        #filename = getNewFilename()
        #pickle.dump(messages, open(filename, 'wb'))
        #convertToCsv.convert(filename)

    def run(self):
        self.join()
        self.msg_loop()
        
            
    def proceed(self, s, t, m):
        ns = State()
        
        ns.t = t        
        ns.tick = s.tick + 1

        for i in range(0,len(m)):
            if m[i]["id"]["name"] == self.name:
                m = m[i]
                break

        ns.lap = m["piecePosition"]["lap"]
        
        ns.x = m["piecePosition"]["inPieceDistance"]
        ns.p = m["piecePosition"]["pieceIndex"]
        ns.a = m["angle"]
        ns.l = m["piecePosition"]["lane"]["endLaneIndex"]  ##FIX THIS IF WE CHANGE LANES (fixed)
        
        ns.w = ns.a - s.a
        if ns.p == s.p: 
            ns.v = ns.x - s.x
        else:
            if "radius" in self.track["pieces"][s.p]:
                ns.v = ns.x + self.track["pieces"][s.p]["length"][s.l] - s.x
            else:
                ns.v = ns.x + self.track["pieces"][s.p]["length"] - s.x
                
        return ns           
    
    def simulate(self, s, t):
        ns = State()
        
        ns.t = t
        ns.p = s.p

        ns.tick = s.tick + 1

        #FIX ME: LAP MISSING
        ns.l = s.l #fix lanes if changed during simulation

        pm = self.track["params"]             

        turboFactor = 1.0
        if self.turbo["on"] == True and self.turbo["startTick"] + self.turbo["turboDurationTicks"] > s.tick:
            turboFactor = self.turbo["turboFactor"]
        
        ns.v = pm["c1"] * s.v + pm["c2"] * s.t * turboFactor
        ns.w = pm["c3"] * s.w + pm["c4"] * s.a * s.v
        if self.track["pieces"][s.p]["curvatureSign"] != 0: 
            ns.w = ns.w + self.track["pieces"][s.p]["curvatureSign"] * max(0, pm["c5"] * s.v + pm["c6.sqrt"] * s.v * s.v * self.track["pieces"][s.p]["curvature.sqrt"][s.l])  
        
        ns.x = s.x + ns.v
        
        pl = 0
        if "radius" in self.track["pieces"][s.p]:
            pl = self.track["pieces"][s.p]["length"][s.l]
        else:
            pl = self.track["pieces"][s.p]["length"]
            
        if(ns.x > pl):
            ns.x = ns.x - pl
            ns.p = (ns.p + 1) % len(self.track["pieces"]) 
        
        ns.a = s.a + ns.w        
        
        #if ns.w > 0:
        #    pdb.set_trace()
                         
        return ns

    def solveTrackParams(self):
        n = len(self.S)
        Av = matrix([[0.0,]*2,]*(n-1))
        tv = [0.0,] * (n-1)
        Aw = matrix([[0.0,]*4,]*(n-1))
        tw = [0.0,] * (n-1)
        curvaturePiece = [False,] * (n-1)
        pieceIndex = [0,] * (n-1)

        gv = []
        gw = []

        c1 = [0.0,] * (n-1)
        c2 = [0.0,] * (n-1)
        c3 = [0.0,] * (n-1)
        c4 = [0.0,] * (n-1)
        c5 = [0.0,] * (n-1)
        c6 = [0.0,] * (n-1)

        for i in range(n - 1):
            curvaturePiece[i] = self.track["pieces"][self.S[i].p]["curvatureSign"] != 0
            pieceIndex[i] = self.S[i].p

            Av[i,0] = self.S[i].v
            Av[i,1] = self.S[i].t
            tv[i] = self.S[i+1].v

            if curvaturePiece[i]:
                Aw[i,0] = self.S[i].w
                Aw[i,1] = self.S[i].a * self.S[i].v
                Aw[i,2] = self.track["pieces"][self.S[i].p]["curvatureSign"] * self.S[i].v
                Aw[i,3] = self.track["pieces"][self.S[i].p]["curvatureSign"] * self.track["pieces"][self.S[i].p]["curvature.sqrt"][self.S[i].l] * self.S[i].v * self.S[i].v
                tw[i] = self.S[i+1].w

        for i in range(6,n-6):
            if(pieceIndex[i] == pieceIndex[i+1] and Av[i,0] > 0 and Av[i+1,0] > 0):
                c1[i], c2[i] = linalg.solve(Av[range(i,i+2),], tv[i:(i+2)])
                gv.append(i)
            if(pieceIndex[i] == pieceIndex[i+3] and curvaturePiece[i] and Av[i,0] > 0 and Av[i+3,0] > 0 and Aw[i,1] != 0):
                c3[i], c4[i], c5[i], c6[i] = linalg.solve(Aw[range(i,i+4),], tw[i:(i+4)])
                gw.append(i)

        print("Keimola true parameters:")
        print(self.track["params"])

        self.track["params"]["c1"] = numpy.median([c1[i] for i in gv])
        self.track["params"]["c2"] = numpy.median([c2[i] for i in gv])
        self.track["params"]["c3"] = numpy.median([c3[i] for i in gw])
        self.track["params"]["c4"] = numpy.median([c4[i] for i in gw])
        self.track["params"]["c5"] = numpy.median([c5[i] for i in gw])
        self.track["params"]["c6"] = numpy.median([c6[i] for i in gw])**2
        self.track["params"]["c6.sqrt"] = numpy.median([c6[i] for i in gw])

        self.fittedFinished = self.prevS

        print("Fitted parameters:")
        print(self.track["params"])

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
        
        

