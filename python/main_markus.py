import json
import socket
import sys
import pickle
import os
import random
import convertToCsv
import math
import pdb
import numpy
import time
from numpy import matrix
from numpy import linalg

def getNewFilename():
    num = 1
    while os.path.exists('messages-' + str(num) + '.p'):
        num = num + 1
    return 'messages-' + str(num) + '.p'

def pS(S):
    print "\n".join(map(repr, S))

class State:
    v = 0
    x = 0
    p = 0
    t = 0
    a = 0
    w = 0    
    l = 0
    tick = -1
    lap = 0

    def dist(self, another):
        return (
            abs(self.v - another.v) +
            abs(self.x - another.x) +
            abs(self.p - another.p) +
            abs(self.t - another.t) +
            abs(self.a - another.a) +
            abs(self.w - another.w) +
            abs(self.l - another.l)
        )

    def __repr__(self):
        return "v: " + str(self.v) + " x: " + str(self.x) +" p: " + str(self.p) +" t: " + str(self.t) + " a: " + str(self.a) +" w: " + str(self.w) +" l: " + str(self.l) 
          
class NoobBot(object):
    
    tick = 0
    prevS = State()
    collectingData = True
    collectingTurboData = True
    #turbo = {"available":False, "on":False}
    turboAvailable = False
    turboStartTick = -3
    turboEndTick = -3
    turboDurationTicks = 0
    turboOn = False
    fittedFinished = State()
    bestStartingPoint = State()
    tickTimes = []
    maxAngle = 30 #inited for fitting # 59.9
    carIndeksoija = {}

    testNextLane = "Left"

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name + random.choice(["a","b","c","d","e"])
        self.key = key
    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def msg(self, msg_type, data, gameTick):
        if gameTick == None:
            sentData = {"msgType": msg_type, "data": data}
        else:
            sentData = {"msgType": msg_type, "data": data, "gameTick": gameTick}
        if msg_type != "ping" or "hello" in data:
            self.send(json.dumps(sentData))
        return sentData

    def join(self):
        finalForCI = False #True
        if finalForCI:
            return self.msg("join", {
                        "name": self.name,
                        "key": self.key
                        }, None)
        else:
            return self.msg("joinRace", {
                    "botId": {
                        "name": self.name,
                        "key": self.key
                        },
                    "trackName":"keimola",
                    #"trackName":"germany",
                    #"trackName":"usa",
                    #"trackName":"france",
                    "carCount": 1
                    }, None)
    def throttle(self, throttle, gameTick):
        return self.msg("throttle", throttle, gameTick)

    def ping(self):
        return self.msg("ping", {}, None)

    def pingTrue(self):
        return self.msg("ping", {"hello": 1}, None)

    def on_join(self, data):
        print("Joined")
        return self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.tick = 1
        self.testS = self.S
        self.testSS = self.SS
        self.S = []
        self.SS = []
        self.prevS = State()
        self.turboAvailable = False
        self.turboStartTick = -3
        self.turboEndTick = -3
        self.turboDurationTicks = 0
        self.carIndeksoija = {}
        self.OS = []

        return self.pingTrue()
    
    def curvedLength(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return correctedRadius * abs(piece['angle'])/360.0 * 2 * math.pi
    
    def curvature(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return 1.0/correctedRadius

    def on_gameinit(self, data):
        self.track = data["race"]["track"]
        self.c1 = 0.98
        self.c2 = 0.2
        self.c3 = 0.9
        self.c4 = -0.00125
        self.c5 = -0.3
        self.c6 = 0.28125
        self.c6sqrt = math.sqrt(self.c6)
        self.S = []
        self.SS = []
        self.OS = []
        
        for piece in self.track["pieces"]:
            piece["curvatureSign"] = 0
            if not("length" in piece):
                if(piece['angle'] > 0):
                    piece["curvatureSign"] = 1
                else:
                    piece["curvatureSign"] = -1
                piece['length'] = [self.curvedLength(piece, lane) for lane in self.track['lanes']]
                piece['curvature'] = [self.curvature(piece, lane) for lane in self.track['lanes']]
                piece['curvature.sqrt'] = [math.sqrt(c) for c in piece['curvature']]
            else:
                piece['curvature.sqrt'] = [0 for lane in self.track['lanes']]
        self.curvatureSigns = [pc["curvatureSign"] for pc in self.track["pieces"]]
        self.pieceLengths = [pc["length"] for pc in self.track["pieces"]]
        self.curvatureSqrts = [pc["curvature.sqrt"] for pc in self.track["pieces"]]
        self.curvatureSqrtMax = [max(self.curvatureSqrts[i]) for i in range(len(self.curvatureSqrts))]
        self.totalPieces = len(self.track["pieces"])

        print("Game inited, track "+self.track["name"])
        return self.ping()

    def throttleToKeepNextVelocity(self, s):
        #pm = self.track["params"]  
        turboFactor = 1.0
        if self.turboEndTick + 1 > s.tick and s.tick > self.turboStartTick - 2:
            turboFactor = self.turboFactor
        return max(0.0, min(1.0, (1-self.c1) * s.v / self.c2 / turboFactor))

    def parseThrottle(self, s, t):
        if t == -1:
            return self.throttleToKeepNextVelocity(s)
        else:
            return t

    def simulateCrash(self, s, T, nt0):
        for t in T:
            s = self.simulate(s, self.parseThrottle(s,t))
            if abs(s.a) > self.maxAngle:
                return {"crash": True}
        s = self.simulate(s, 0)
        if abs(s.a) > self.maxAngle:
            return {"crash": True}
        s = self.simulate(s, 0)
        if abs(s.a) > self.maxAngle:
            return {"crash": True}
        fs = s
        for i in range(nt0):
            s = self.simulate(s, 0)
            if abs(s.a) > self.maxAngle:
                return {"crash": True}
        return {"crash": False, "finalState": fs}
    
    def on_lapfinished(self,data):
        print "Lap time: ",data["lapTime"]["millis"]*0.001
        return self.ping()

    def on_car_positions(self, data):
        self.tick += 1
        startTime = time.time()

        if(self.prevS.v == 0):
            throttle_value = 1.0
        else:
            #BN = 4
            #BL = [1,3,3,3]
            BN = 2
            BL = [1,5]

            #BL = [5, ] * BN
            T = [-1,] * sum(BL)
            #T = [0,] * sum(BL)

            Bt = -1
            BS = self.prevS

            tl = len(self.track["pieces"]) 

            M = 3**BN;
            for k in range(M):
                kk = k
                bi = 0
                for b in range(BN):
                    t = kk % 3 - 1
                    kk /= 3
                    for i in range(BL[b]):
                        T[bi + i] = t
                    bi += BL[b]

                sc = self.simulateCrash(self.prevS, T, 300)
                if not sc["crash"]:
                    sc = sc["finalState"]
                    p1 = (BS.p - self.prevS.p + tl) % tl 
                    p2 = (sc.p - self.prevS.p + tl) % tl 
                    if(p2 > p1 or (p2 == p1 and sc.x > BS.x)):
                        BS = sc
                        Bt = T[0]

            throttle_value = self.parseThrottle(self.prevS, Bt)

        #if not self.simulateCrash(self.prevS, 1.0):
        #    throttle_value = 1.0
        #else:
        #    tmin = 0.0
        #    tmax = 1.0
        #    while tmax - tmin > 0.001:
        #        pivot = (tmin+tmax)*0.5
        #        if(self.simulateCrash(self.prevS, pivot)):
        #            tmax = pivot
        #        else:
        #            tmin = pivot
        #    throttle_value = tmin            
        
        self.append_OS(data)
        
        if(self.collectingData): #fit model
            throttle_value *= random.uniform(0.99, 1.0)

        prevPrevS = self.prevS

        self.SS.append(self.simulate(self.prevS, throttle_value))
        
        self.prevS = self.proceed(self.prevS, throttle_value, data, self.name)
        self.S.append(self.prevS)

        if(self.collectingData): #fit model
            self.addDataToFit()

        if(self.tick % 50 == 0 or (not self.collectingData and self.S[-1].dist(self.SS[-1]) > 0.0001 and self.prevS.v > 0)):
            print str(self.tick) + ": " + str(self.S[-1].dist(self.SS[-1]))
        #if not(self.collectingData) and self.S[-1].dist(self.SS[-1]) > 0.35:
        #    pdb.set_trace()

        if not self.collectingData and self.collectingTurboData:
            if (self.fittedFinished.lap + 1 == self.prevS.lap and self.fittedFinished.p + 2 < self.prevS.p) or (self.fittedFinished.lap + 2 <= self.prevS.lap):
                self.optimize_turbo()

        thisTickTime = 1000 * (time.time() - startTime)
        self.tickTimes.append(thisTickTime)
        if(self.tick % 50 == 0):
            print str(self.tick) + ": " + str(thisTickTime) + " ms, median " + str(numpy.median(self.tickTimes)) + " ms, max " + str(max(self.tickTimes)) + " ms"
        
        if self.testNextLane == "Right" and self.prevS.l == 1 or self.testNextLane == "Left" and self.prevS.l == 0 : #FIX ME: REMOVE
            self.prevS.t = prevPrevS.t
            if self.prevS.l == 1:
                self.testNextLane = "Left"
            else:
                self.testNextLane = "Right"
            return self.switch_lane(self.testNextLane, self.tick-1)
        elif not self.collectingTurboData and self.turboAvailable and throttle_value > 0.99 and ((self.prevS.p == self.bestStartingPoint.p and self.prevS.x > self.bestStartingPoint.x) or self.prevS.p == (self.bestStartingPoint.p + 1) % len(self.track["pieces"])):
            self.prevS.t = prevPrevS.t
            return self.turbodo() 
        else:
            return self.throttle(throttle_value, self.tick - 1)

    def switch_lane(self, slane, gameTick):
        print("Switching lane")
        return self.msg("switchLane", slane, gameTick)

    def on_crash(self, data):
        print("Someone crashed, with prev angle")
        print(self.prevS.a)
        return self.ping()

    def on_turbostart(self, data):
        self.turboOn = True
        self.turboStartTick = self.tick
        self.turboEndTick = self.tick + self.turboDurationTicks
        self.turboAvailable = False
        return self.ping()
    
    def on_turboend(self, data):
        self.turboOn = False
        return self.ping()
    
    def turbodo(self):
        self.turboAvailable = False
        print("started turbo")
        return self.msg("turbo", "wroom wroom", self.tick-1)
 
    def on_turboavailable(self, data):
        if not self.turboOn:
            self.turboAvailable = True
            self.turboOn = False
            self.turboDurationTicks = data['turboDurationTicks']
            self.turboFactor = data['turboFactor']
            self.turboDurationMilliseconds = data['turboDurationMilliseconds']
        print("Turbo is available")

    def optimize_turbo(self):
        l = -1
        bi = 1
        bl = 0
        ok = False
        for i in range(self.fittedFinished.tick+2, len(self.S)):
            if(self.S[i].t != 1.0):
                ok = True

            if(ok):
                if(self.S[i].t == 1.0):
                    if(l == -1):
                        l=1
                    else:
                        l+=1
                else:
                    if(l > bl):
                        bi = i - l
                        bl = l
                    l = -1
        self.bestStartingPoint = self.S[bi]

        self.collectingTurboData = False

        print(self.S[bi])

        return self.ping()

    def on_game_end(self, data):
        print("Race ended")
        return self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        return self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_gameinit,
            'lapFinished': self.on_lapfinished,
            'turboAvailable': self.on_turboavailable,
            'turboStart': self.on_turbostart,
            'turboEnd': self.on_turboend
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        messages = []
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            #if 'gameTick' in msg and data != None and msg_type == 'carPositions':
            #    data[0]['gameTick'] = msg['gameTick']
            if msg_type in msg_map:
                return_msg = msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                return_msg = self.ping()
            messages.append({'msg': msg, 'return': return_msg})
            line = socket_file.readline()
        filename = getNewFilename()
        pickle.dump(messages, open(filename, 'wb'))
        convertToCsv.convert(filename)

    def run(self):
        self.join()
        self.msg_loop()
        
    
    def append_OS(self, data):
        for i in range(len(data)):
            name = data[i]["id"]["name"]
            if name != self.name:
                prevs = State()
                
                if not (name in self.carIndeksoija):
                    self.carIndeksoija[name] = len(self.carIndeksoija)
                    self.OS.append([])
                else:
                    prevs = self.OS[self.carIndeksoija[name]][-1]
                
                self.OS[self.carIndeksoija[name]].append(self.proceed(prevs,0,data,name))
   
    def proceed(self, s, t, m, name):
        ns = State()
        
        ns.t = t        
        ns.tick = s.tick + 1

    	for i in range(0,len(m)):
            if m[i]["id"]["name"] == name:
                m = m[i]
                break

        ns.lap = m["piecePosition"]["lap"]
        
        ns.x = m["piecePosition"]["inPieceDistance"]
        ns.p = m["piecePosition"]["pieceIndex"]
        ns.a = m["angle"]
        ns.l = m["piecePosition"]["lane"]["endLaneIndex"] #s.l ##FIX THIS IF WE CHANGE LANES - is it fixed??
        
        ns.w = ns.a - s.a
        if ns.p == s.p: 
            ns.v = ns.x - s.x
        else:
            if "radius" in self.track["pieces"][s.p]:
                ns.v = ns.x + self.track["pieces"][s.p]["length"][s.l] - s.x
            else:
                ns.v = ns.x + self.track["pieces"][s.p]["length"] - s.x
                
        return ns           
    
    def simulate(self, s, t):
        ns = State()
        
        ns.t = t
        ns.p = s.p

        ns.l = s.l #fix lanes if changed during simulation

        ns.tick = s.tick + 1

        #FIX ME: LAP MISSING
        
        turboFactor = 1.0
        if self.turboEndTick + 1 > s.tick and s.tick > self.turboStartTick - 2:
            turboFactor = self.turboFactor
        
        ns.v = self.c1 * s.v + self.c2 * s.t * turboFactor
        ns.w = self.c3 * s.w + self.c4 * s.a * s.v
        if self.curvatureSigns[s.p] != 0: 
            ns.w = ns.w + self.curvatureSigns[s.p] * max(0, self.c5 * s.v + self.c6sqrt * s.v * s.v * self.curvatureSqrts[s.p][s.l])  
        
        ns.x = s.x + ns.v
        
        pl = 0
        if self.curvatureSigns[s.p] != 0:
            pl = self.pieceLengths[s.p][s.l]
        else:
            pl = self.pieceLengths[s.p]
            
        if(ns.x > pl):
            ns.x = ns.x - pl
            ns.p = (ns.p + 1) % self.totalPieces
        
        ns.a = s.a + ns.w        
        
        return ns

    #parameters for collecting fitting data, not optimized :)
    fitMaxN = 1000
    fiti = -1
    fitAv = matrix([[0.0,]*2,]*(fitMaxN-1))
    fittv = [0.0,] * (fitMaxN-1)
    fitAw = matrix([[0.0,]*4,]*(fitMaxN-1))
    fittw = [0.0,] * (fitMaxN-1)
    fitCurvaturePiece = [False,] * (fitMaxN-1)
    fitPieceIndex = [0,] * (fitMaxN-1)
    fitGV = []
    fitGW = []
    fitGVn = 0
    fitGWn = 0
    fitc1 = [0.0,] * (fitMaxN-1)
    fitc2 = [0.0,] * (fitMaxN-1)
    fitc3 = [0.0,] * (fitMaxN-1)
    fitc4 = [0.0,] * (fitMaxN-1)
    fitc5 = [0.0,] * (fitMaxN-1)
    fitc6 = [0.0,] * (fitMaxN-1)

    def addDataToFit(self):
        if self.fiti == -1:
            self.fiti += 1
            return

        if(self.fiti > 0):
            self.fittv[self.fiti-1] = self.S[self.fiti].v
            self.fittw[self.fiti-1] = self.S[self.fiti].w

            if(self.fiti - 2 >= 5):
                ii = self.fiti - 2
                if(self.fitPieceIndex[ii] == self.fitPieceIndex[ii+1] and self.fitAv[ii,0] > 0 and self.fitAv[ii+1,0] > 0 and self.fitAv[ii,1] > 0 and self.fitAv[ii + 1,1] > 0):
                    self.fitc1[ii], self.fitc2[ii] = linalg.solve(self.fitAv[range(ii,ii+2),], self.fittv[ii:(ii+2)])
                    self.fitGV.append(ii)
                    self.fitGVn += 1
            if(self.fiti - 4 >= 5):
                ii = self.fiti - 4
                if(self.fitPieceIndex[ii] == self.fitPieceIndex[ii+3] and self.fitCurvaturePiece[ii] and self.fitAv[ii,0] > 0 and self.fitAv[ii+3,0] > 0 and self.fitAw[ii,1] != 0):
                    self.fitc3[ii], self.fitc4[ii], self.fitc5[ii], self.fitc6[ii] = linalg.solve(self.fitAw[range(ii,ii+4),], self.fittw[ii:(ii+4)])
                    self.fitGW.append(ii)
                    self.fitGWn += 1

        if self.fitGVn > 5:
            self.c1 = numpy.median([self.fitc1[i] for i in self.fitGV])
            self.c2 = numpy.median([self.fitc2[i] for i in self.fitGV])

        if self.fitGWn > 7 and self.fitGVn > 5:
            self.c3 = numpy.median([self.fitc3[i] for i in self.fitGW])
            self.c4 = numpy.median([self.fitc4[i] for i in self.fitGW])
            self.c5 = numpy.median([self.fitc5[i] for i in self.fitGW])
            self.c6 = numpy.median([self.fitc6[i] for i in self.fitGW])**2
            self.c6sqrt = numpy.median([self.fitc6[i] for i in self.fitGW])
            print("Fitted all equations, stop fitting")
            print(str([self.c1, self.c2, self.c3, self.c4, self.c5, self.c6, self.c6sqrt]))
            self.collectingData = False
            self.fittedFinished = self.prevS
            self.maxAngle = 59.9999

        #add raw data
        s = self.S[self.fiti]
        self.fitCurvaturePiece[self.fiti] = self.curvatureSigns[s.p] != 0
        self.fitPieceIndex[self.fiti] = s.p

        self.fitAv[self.fiti,0] = s.v
        self.fitAv[self.fiti,1] = s.t
        if self.fitCurvaturePiece[self.fiti]:
            self.fitAw[self.fiti,0] = s.w
            self.fitAw[self.fiti,1] = s.a * s.v
            self.fitAw[self.fiti,2] = self.curvatureSigns[s.p] * s.v
            self.fitAw[self.fiti,3] = self.curvatureSigns[s.p] * self.curvatureSqrts[s.p][s.l] * s.v * s.v
        
        self.fiti += 1

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
