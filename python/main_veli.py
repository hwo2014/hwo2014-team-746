import json
import socket
import sys
import pickle
import os
import random
import convertToCsv
import math
import pdb

def getNewFilename():
    num = 1
    while os.path.exists('messages-' + str(num) + '.p'):
        num = num + 1
    return 'messages-' + str(num) + '.p'

def pS(S):
    print "\n".join(map(repr, S))

class State:
    v = 0
    x = 0
    p = 0
    t = 0
    tp = 0
    a = 0
    w = 0    
    l = 0

    def dist(self, another):
        return (
            abs(self.v - another.v) +
            abs(self.x - another.x) +
            abs(self.p - another.p) +
            abs(self.t - another.t) +
            abs(self.tp - another.tp) +
            abs(self.a - another.a) +
            abs(self.w - another.w) +
            abs(self.l - another.l)
        )

    def __repr__(self):
        return "v: " + str(self.v) + " x: " + str(self.x) +" p: " + str(self.p) +" t: " + str(self.t) +" tp: " + str(self.tp) +" a: " + str(self.a) +" w: " + str(self.w) +" l: " + str(self.l) 
          
class NoobBot(object):
    
    tick = 0
    prevS = State()

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def msg(self, msg_type, data):
        sentData = {"msgType": msg_type, "data": data}
        self.send(json.dumps(sentData))
        return sentData

    def join(self):
        return self.msg("joinRace", {
	  "botId": {
	    "name": self.name,
	    "key": self.key
	  },
	  "trackName":"keimola",
	  "carCount": 1
	})
    def throttle(self, throttle):
        return self.msg("throttle", throttle)

    def ping(self):
        return self.msg("ping", {})

    def on_join(self, data):
        print("Joined")
        return self.ping()

    def on_game_start(self, data):
        print("Race started")
        return self.ping()    
    
    def curvedLength(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return correctedRadius * abs(piece['angle'])/360.0 * 2 * math.pi
    
    def curvature(self, piece, lane):
        correctedRadius = piece['radius'] - piece["curvatureSign"] * lane['distanceFromCenter']
        return 1.0/correctedRadius

    def on_gameinit(self, data):
        self.track = data["race"]["track"]
        self.track["params"] = { "c1": 0.98, "c2": 0.2, "c3": 0.9, "c4": -0.00125, "c5": -0.3, "c6": 0.28125 }
        self.S = []
        self.SS = []
        for piece in self.track["pieces"]:
            piece["curvatureSign"] = 0
            if not("length" in piece):
                if(piece['angle'] > 0):
                    piece["curvatureSign"] = 1
                else:
                    piece["curvatureSign"] = -1
                piece['length'] = [self.curvedLength(piece, lane) for lane in self.track['lanes']]
                piece['curvature'] = [self.curvature(piece, lane) for lane in self.track['lanes']]
        print("Game inited, track "+self.track["name"])
        return self.ping()

    def simulateCrash(self, s, t):
        s = self.simulate(s, t)
        if(abs(s.a) > 59.99):
            return True
        for i in range(300):
            s = self.simulate(s, 0.0)
            if(abs(s.a) > 59.99):
                return True
        return False
    
    def on_car_positions(self, data):
        self.tick += 1
        
        if not self.simulateCrash(self.prevS, 1.0):
            throttle_value = 1.0
        else:
            throttle_value = 0.0
            #tmin = 0.0
            #tmax = 1.0
            #while tmax - tmin > 0.001:
            #    pivot = (tmin+tmax)*0.5
            #    if(self.simulateCrash(self.prevS, pivot)):
            #        tmax = pivot
            #    else:
            #        tmin = pivot
            #throttle_value = tmin            
        
        self.SS.append(self.simulate(self.prevS, throttle_value))
        
        self.prevS = self.proceed(self.prevS, throttle_value, data)
        self.S.append(self.prevS)

        print str(self.tick) + ": " + str(self.S[-1].dist(self.SS[-1]))
        if self.S[-1].dist(self.SS[-1]) > 0.01:
            pdb.set_trace()
        
        return self.throttle(throttle_value)

    def on_crash(self, data):
        print("Someone crashed")
        return self.ping()

    def on_game_end(self, data):
        print("Race ended")
        return self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        return self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
	        'gameInit': self.on_gameinit
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        messages = []
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                return_msg = msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                return_msg = self.ping()
            messages.append({'msg': msg, 'return': return_msg})
            line = socket_file.readline()
        filename = getNewFilename()
        pickle.dump(messages, open(filename, 'wb'))
        convertToCsv.convert(filename)

    def run(self):
        self.join()
        self.msg_loop()
        
            
    def proceed(self, s, t, m):
        ns = State()
        
        ns.t = t        
        ns.tp = s.t
        
        m = m[0] ##CHANGE WHEN MULTIPLE CARS
        
        ns.x = m["piecePosition"]["inPieceDistance"]
        ns.p = m["piecePosition"]["pieceIndex"]
        ns.a = m["angle"]
        ns.l = s.l ##FIX THIS IF WE CHANGE LANES
        
        ns.w = ns.a - s.a
        if ns.p == s.p: 
            ns.v = ns.x - s.x
        else:
            if "radius" in self.track["pieces"][s.p]:
                ns.v = ns.x + self.track["pieces"][s.p]["length"][s.l] - s.x
            else:
                ns.v = ns.x + self.track["pieces"][s.p]["length"] - s.x
                
        return ns           
    
    def simulate(self, s, t):
        ns = State()
        
        ns.t = t
        ns.tp = s.t
        ns.p = s.p
        
        pm = self.track["params"]                                 
        
        ns.v = pm["c1"] * s.v + pm["c2"] * s.tp
        ns.w = pm["c3"] * s.w + pm["c4"] * s.a * s.v
        if self.track["pieces"][s.p]["curvatureSign"] != 0: 
            ns.w = ns.w + self.track["pieces"][s.p]["curvatureSign"] * max(0, pm["c5"] * s.v + math.sqrt(pm["c6"]) * s.v * s.v * math.sqrt(self.track["pieces"][s.p]["curvature"][s.l]))  
        
        ns.x = s.x + ns.v
        
        pl = 0
        if "radius" in self.track["pieces"][s.p]:
            pl = self.track["pieces"][s.p]["length"][s.l]
        else:
            pl = self.track["pieces"][s.p]["length"]
            
        if(ns.x > pl):
            ns.x = ns.x - pl
            ns.p = (ns.p + 1) % len(self.track["pieces"]) 
        
        ns.a = s.a + ns.w        
        
        #if ns.w > 0:
        #    pdb.set_trace()
                
        return ns

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
        
        

