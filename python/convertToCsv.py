#!/usr/bin/python

import pickle
import csv
#from pylab import *

# Return the only element of a list.
def only(lst):
    assert len(lst) == 1
    return lst[0]

def flatRow(msg):
    # Just one car
    carposData = only(msg['msg']['data'])
    assert msg['return']['msgType'] == 'throttle'
    return [
        carposData['angle'],
        carposData['piecePosition']['pieceIndex'],
        carposData['piecePosition']['inPieceDistance'],
        carposData['piecePosition']['lane']['startLaneIndex'],
        carposData['piecePosition']['lane']['endLaneIndex'],
        carposData['piecePosition']['lap'],
        msg['return']['data']
    ]

# Convert filename.p to filename.csv
def convert(filename):
    assert filename.endswith('.p')
    allMessages = pickle.load(open(filename, 'rb'))

    carPosMessages = [msg for msg in allMessages if msg['msg']['msgType'] == 'carPositions']

    headerRow = [
        'angle',
        'pieceIndex',
        'inPieceDistance',
        'startLaneIndex',
        'endLaneIndex',
        'lap',
        'throttle'
    ]

    rows = [headerRow] + [flatRow(msg) for msg in carPosMessages if msg['return']['msgType'] == 'throttle']

    # Transform filename.p -> filename.csv
    cvsFilename = filename[:-1] + "csv"
    
    with open(cvsFilename, 'wb') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(rows)
