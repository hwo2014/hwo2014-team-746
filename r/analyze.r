options(width=200) 

d <- rbind(read.csv("../python/messages-1-constant-0.6.csv"), read.csv("../python/messages-2-constant-0.5.csv"),
           read.csv("../python/messages-3-0.65-and-0.0.csv"), read.csv("../python/messages-4-two-random.csv"),
           read.csv("../python/messages-5-1.0.csv"), read.csv("../python/messages-6-0.9.csv"))

d <- read.csv("germany.csv")
d <- read.csv("messages-11.csv")

d <- read.csv("usa.csv")

n <- nrow(d)
n

library(rjson)

track <- fromJSON(file="../python/keimola.json")

track <- fromJSON(file="germany.json")$race$track

track <- fromJSON(file="usa.json")$race$track


d$inPieceDistanceT <- c(d$inPieceDistance[-1], NA) - d$inPieceDistance
d$inPieceDistance1 <- d$inPieceDistance - c(NA, d$inPieceDistance[-n])
d$inPieceDistance2 <- d$inPieceDistance - c(NA, NA, d$inPieceDistance[c(-n+1, -n)]) 
d$inPieceDistance3 <- d$inPieceDistance - c(NA, NA, NA, d$inPieceDistance[c(-n+2, -n+1, -n)])
d$inPieceDistance1[d$inPieceDistance1 < 0] <- NA
d$inPieceDistance2[d$inPieceDistance2 < 0] <- NA
d$inPieceDistance3[d$inPieceDistance3 < 0] <- NA
d$inPieceDistanceT[d$inPieceDistanceT < 0] <- NA
d$inPieceDistance12 <- d$inPieceDistance1 - d$inPieceDistance2
d$throttle1 <- c(NA, d$throttle[-n])
d$throttle1[is.na(d$inPieceDistance1)] <- NA
d$throttle2 <- c(NA, NA, d$throttle[c(-n+1,-n)])
d$throttle2[is.na(d$inPieceDistance2)] <- NA

#d$inPieceDistanceT[d$inPieceDistanceT == 0 & 1:nrow(d) > 8000] <- NA

plot(d$inPieceDistanceT)

#m <- lm(inPieceDistanceT ~ throttle * throttle1 * throttle2 * inPieceDistance1 * I(inPieceDistance1^2) * inPieceDistance2 * I(inPieceDistance2^2), data = d[6455:6470,]) # + inPi
#plot(predict(m,d) - d$inPieceDistanceT)
#plot(predict(m,d) - d$inPieceDistanceT, ylim=c(-0.001,0.001))
#plot(predict(m,d) - d$inPieceDistanceT, ylim=c(-10,10))
#summary(m)

#PERFECT MATCH, train with car speeding from 0 with random throttles
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[6455:6460,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[105:110,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[405:410,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[05:7,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[6:7,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[10:11,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[15:20,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[800:820,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d[d$pieceIndex==21,]) # + inPi
m <- lm(inPieceDistanceT ~ 0 + throttle1 + inPieceDistance1, data = d) # + inPi
plot(predict(m,d) - d$inPieceDistanceT)
plot(predict(m,d) - d$inPieceDistanceT, ylim=c(-0.001,0.001))
summary(m)
#new speed = a * prev throttle + b * current speed

par(mfcol=c(3,1))
plot(predict(m,d) - d$inPieceDistanceT)
plot(d$radius)
plot(d$pieceIndex)


dd <- d
dd$throttle1 <- dd$throttle2
plot(predict(m,dd) - dd$inPieceDistanceT)

dd <- d
dd$throttle1 <- dd$throttle3
plot(predict(m,dd) - dd$inPieceDistanceT)


plot(predict(m,d),type="l", col="red")
lines(d$inPieceDistanceT, col="green")

plot(predict(m,d),type="l", col="red",xlim=c(7500,8000),ylim=c(5.9,6.1))
lines(d$inPieceDistanceT, col="green")


#angle

plot(d$angle)


radius <- sapply(track$pieces, function(t) { ifelse(is.null(t$radius), NA, t$radius) })
track.angle <- sapply(track$pieces, function(t) { ifelse(is.null(t$angle), 0, t$angle) })
curvature.sign <- ifelse(track.angle == 0, 0, ifelse(track.angle > 0, 1, -1))

d$radius <- radius[d$pieceIndex+1]
d$curvature.sign <- curvature.sign[d$pieceIndex+1]

d$radius <- d$curvature.sign * d$radius
d$radius <- d$radius + 10 ###MANUAL FIX FOR LANE 0

#d$track.angle
d$curvature <- ifelse(is.na(d$radius), 0, 1/d$radius)
d$curvature.sqrt <- d$curvature.sign * sqrt(abs(d$curvature))

#d$curvature <- d$curvature * d$curvature.sign

plot(d$radius, type="l")
lines(d$angle, col="red")

d$angleT <- c(d$angle[-1], NA) - d$angle
d$angle1 <- d$angle - c(NA, d$angle[-n])
d$angle2 <- d$angle - c(NA,NA, d$angle[c(-n+1,-n)])
d$curvature1 <- c(NA, d$curvature[-n])
d$angleT[is.na(d$inPieceDistanceT)] <- NA

d$inPieceDistance1.55 <- d$inPieceDistance1 > 5.5
d$inPieceDistance1.60 <- d$inPieceDistance1 > 6.0

#d$curvature <- d$curvature.sign * d$curvature
#d$angleT <- d$curvature.sign * d$angleT
#d$angle1 <- d$curvature.sign * d$angle1
#d$angle <- d$curvature.sign * d$angle

m <- lm(angleT ~ 1, data = d) #[6500:7500,], subset = abs(angleT) > 0.01)
summary(m)

m <- lm(angleT ~ angle * angle1 * curvature * I(curvature^2) * throttle * throttle1 * inPieceDistance1, data = d[6500:7500,], subset = abs(angleT) > 0.1)
summary(m)

m <- lm(angleT ~ angle * angle1 * curvature * curvature1 * I(curvature^2) * inPieceDistance1 * inPieceDistance1.55, data = d)#[6500:7500,])#, subset = abs(angleT) > 0.1)
summary(m)


m <- lm(angleT ~ angle + angle1 * curvature + I(curvature^2) + throttle + throttle1 + inPieceDistance1 + inPieceDistance2, data = d[6500:7500,], subset = abs(angleT) > 0.01)
summary(m)

m <- lm(angleT ~ angle + angle1 + curvature*inPieceDistance1*inPieceDistance1.55 + I(curvature^2) + throttle1 + throttle2 + inPieceDistance1 + inPieceDistance2, data = d[6500:7500,])#, subset = abs(angleT) > 0.01)
summary(m)


m <- lm(angleT ~ angle + angle1+inPieceDistance1 * curvature + I(curvature^2), data = d[6500:7500,], subset = abs(angleT) > 0.01)
summary(m)

par(mfcol=c(2,2))
i <- 6500:7500
i <- 6800:6900
plot(d$angle[i])
plot(d$angle1[i])
plot(d$inPieceDistance1[i])
plot(d$curvature[i])
#different properties per curvature tiles!!


dev.off()

plot(predict(m,d) - d$angleT)
lines(d$angleT, col="red")
#new speed = a * prev throttle + b * current speed

plot(predict(m,d),type="l", col="red", ylim=c(-10,10))
lines(d$angleT, col="green")

plot(d$angleT, col="green", type="l")

plot(d$inPieceDistance1[d$curvature > 0], d$angleT[d$curvature > 0])
plot(d$throttle[d$curvature > 0], d$angleT[d$curvature > 0])

plot(d$inPieceDistance1[d$curvature == 0.005], d$angleT[d$curvature == 0.005])
plot(d$inPieceDistance1[d$curvature == 0.01], d$angleT[d$curvature == 0.01])

table(d$curvature)

i <- which(d$angle!=0 & 1:nrow(d) > 9000)
plot(d$inPieceDistance1[i])
lines((d$curvature[i]+0.015)*380)
#lines(d$angle[i],col="red")

i <- which(d$angle != 0 & c(FALSE, d$angle[-n] == 0 & !is.na(d$angle[-n])) & c(FALSE, d$curvature[-n] != 0 & !is.na(d$curvature[-n])))
#i <- which(d$angle!=0 & 1:nrow(d) > 9000)

d$inPieceDistance1[i[d$curvature[i] == 0.005]]

d$inPieceDistance1[i[d$curvature[i] == 0.01]]

d$inPieceDistance1[i[d$curvature[i] == 0.01] - 1]

d$inPieceDistance1[i[d$curvature[i] == 0.01] - 1]

d$inPieceDistance[i[d$curvature[i] == 0.01] - 1]

table(d$inPieceDistance1[i[d$inPieceDistance[i] > 60 & d$curvature[i] == 0.01] ])

table(d$inPieceDistance1[i[d$inPieceDistance[i] > 60 & d$curvature[i] == 0.01] - 1])

d$inPieceDistance1[i[d$inPieceDistance[i] > 30 & d$curvature[i] == 0.01 & d$throttle[i]==1] ]

d$inPieceDistance1[i[d$inPieceDistance[i] > 30 & d$curvature[i] == 0.01 & d$throttle[i]==1] ]
d$pieceIndex[i[d$inPieceDistance[i] > 30 & d$curvature[i] == 0.01 & d$throttle[i]==1] ]
d$angle[i[d$inPieceDistance[i] > 30 & d$curvature[i] == 0.01 & d$throttle[i]==1] ]

#curvature tiles have diffent frictions for drift


#d$throttle[i[d$inPieceDistance[i] > 30 & d$curvature[i] == 0.01] ]

summary(lm(angleT ~ 1, data = d[6830:6845,]))


par(mfcol=c(2,2))
i <- 6500:7500
i <- 6800:6900
plot(d$angle[i])
plot(d$angle1[i])
plot(d$inPieceDistance1[i])
plot(d$curvature[i])
#different properties per curvature tiles!!

dev.off()

#PERFECT MATCH INSIDE ONE PIECE WITH ZERO CURVATURE, piece 18!
m <- lm(angleT ~ 0 + angle1 + angle:inPieceDistance1, data = d[6830:6845,])
summary(m)

m <- lm(angleT ~ 0 + angle1 + angle:inPieceDistance1, data = d, subset = curvature == 0)
summary(m)

d$angleT.res <- d$angleT - (0.9*d$angle1 - 0.00125 * d$angle * d$inPieceDistance1)

#curvature 0.1, one single piece 19, practically perfect
summary(lm(angleT ~ 1, data = d[6845:6857,]))
m <- lm(angleT ~ angle1 + angle:inPieceDistance1, data = d[6845:6857,])
summary(m)

#THIS IS PERFECT (not exactly!!)
m <- lm(angleT ~ angle1 + angle:inPieceDistance1 + inPieceDistance1, data = d[6845:6857,])
summary(m)

m <- lm(angleT ~ angle1 + angle:inPieceDistance1 + , data = d[6845:6857,])
summary(m)

i <- d$pieceIndex == 19 & d$angle != 0
i <- d$pieceIndex == 14 & d$angle != 0
i <- abs(d$curvature) == 0.01 & d$angle != 0 & d$inPieceDistance1 > 5.5
#4  5  6  7 14 15 16 17 19 20 21 22 26 27 29 30 31 32 33 34
i <- d$pieceIndex == 31 & d$angle != 0 & d$inPieceDistance1 > 6.0
i <- abs(d$curvature) > 0 & abs(d$curvature) < 8 & d$angle != 0 & d$inPieceDistance1 > 6.0
#i <- d$pieceIndex == 11 & d$angle != 0 & d$inPieceDistance1 > 6.0 #8,11,23,24
m <- lm(angleT ~ angle1 + angle:inPieceDistance1 + inPieceDistance1*angle1*inPieceDistance2, data = d[i,])
m <- lm(angleT.res ~ 1, data = d[i,])
m <- lm(angleT.res ~ inPieceDistance1, data = d[i,])
m <- lm(angleT.res ~ 0 + curvature.sign : inPieceDistance1 + curvature.sign : I(inPieceDistance1^2), data = d[i,])   ###THIS IS PERFECT PERFECT, WHEN ANGLE > 0
m <- lm(angleT.res ~ 0 + curvature.sign : inPieceDistance1 + curvature.sqrt : I(inPieceDistance1^2), data = d[i,])  ##this matches with different curvatures
##4,5,6,7,19,20,21,22,26,27,31,32,33,34 -0.3 + 0.05056 ##ja n�m� curvature 0.01
##14,15,16,17,29,30: -0.3 + 0.05590   ##n�m� kaikki on curvature -0.01
summary(m)

#PERFECT ^3
f <- function(dd) { 0.9*dd$angle1 - 0.00125 * dd$angle * dd$inPieceDistance1 + dd$curvature.sign * pmax(0, -0.3 * dd$inPieceDistance1 + sqrt(0.28125) * dd$curvature.sign * dd$curvature.sqrt * dd$inPieceDistance1^2) }

#how to reach certain angle
ds <- data.frame(angle1 = 10, angle = 0, inPieceDistance1 = 6.78, curvature.sign=1, curvature.sqrt = sqrt(0.01))
for(i in 2:150){
  ds[i,] <- ds[i-1,]
  ds$angle1[i] <- f(ds[i,])
  ds$angle[i] <- ds$angle[i] + ds$angle1[i]
}
plot(ds$angle)

# max speed in curve
cv = 0.01
a = 0.98
b = 0.2
c = 0.9
d = -0.00125
e = -0.3
f = 0.53033008588991081833
v.max <- (-d*60 + -e)/f/sqrt(cv)
(v.max-b)/a

dv <- data.frame(wn=0,w=0,angle=60,v=v.max)
for(i in 2:25){
  dv[i,] <- dv[i-1,]
  dv$wn[i] <- dv$w[i-1]
  dv$v[i] <- (dv$v[i-1]-b)/a
  dv$angle[i] <- dv$angle[i-1]-dv$wn[i]
  dv$w[i] <- (dv$wn[i] - d*dv$angle[i]*dv$v[i] - e*dv$v[i] -f*dv$v[i]^2*sqrt(cv))/c
}
par(mfcol=c(2,2))
plot(dv$v)
plot(dv$angle)
plot(dv$w)

d$predicted.angleT <- f(d)
plot(d$predicted.angleT - d$angleT)

#PERFECT: INPIECEDISTANCE1 > 0.6 -> ERI MALLI

par(mfcol=c(2,2))
plot(d$inPieceDistance1[i])
plot(d$angle[i])
plot(d$angleT[i])
#lines(d$angleT[i]-d$angleT.res[i], col="red")
lines(d$angleT[i]-d$angleT.res[i] + predict(m,d[i, ]), col="red")
plot(d$angleT.res[i] - predict(m,d[i,]))


i <- d$pieceIndex == 31 & d$angle != 0 & d$inPieceDistance1 < 6.0

par(mfcol=c(2,2))
plot(d$inPieceDistance1[i])
plot(d$angle[i])
plot(d$angleT[i])
#lines(d$angleT[i]-d$angleT.res[i], col="red")
lines(d$angleT[i]-d$angleT.res[i] + predict(m,d[i, ]), col="red")
plot(d$angleT.res[i] - predict(m,d[i,]))
lines(d$angleT.res[i],col="red")



d$tick = 1:nrow(d)
m <- lm(angleT.res ~ 1, data = d[6845:6857,])
m <- lm(angleT.res ~ tick, data = d[6845:6857,])
m <- lm(angleT.res ~ inPieceDistance1, data = d[6845:6857,])
summary(m)

i <- 6845:6857
plot(d$angleT.res[i])
lines(predict(m, d[i,]))

plot(d$inPieceDistance1[i])

m <- lm(angleT ~ angle1 + angle:inPieceDistance1 + inPieceDistance1, data = d[6845:6900,])
summary(m)



#the weights for angle1 and angle:inPieceDistance1 are the same!!

#
m <- lm(angleT ~ 0 + factor(pieceIndex) + angle1 + angle:inPieceDistance1, data = d[6800:6900,])
#m <- lm(angleT ~ 0 + I(curvature) + I(curvature^2) + angle1 + angle:inPieceDistance1, data = d[6800:6900,])
summary(m)

-4.098e-01 / 1.857e-01

#THIS IS A GOOD MODEL
summary(lm(angleT ~ 1, data = d))
m <- lm(angleT ~ 0 + factor(pieceIndex) + angle1 + angle:inPieceDistance1, data = d)
#m <- lm(angleT ~ 0 + I(curvature) + I(curvature^2) + angle1 + angle:inPieceDistance1, data = d[6800:6900,])
summary(m)

#AS WELL AS THIS
m <- lm(angleT ~ 0 + curvature + angle1 + angle:inPieceDistance1, data = d)
summary(m)

m <- lm(angleT ~ 0 + curvature:inPieceDistance1:inPieceDistance1.55 + angle1 + angle:inPieceDistance1, data = d)
summary(m)


#new try - THIS IS VERY GOOD, USE THIS! NOT ENOUGH DATA ON CURVATURE YET, NOTE! REMEMBER THE RANDOMNESS PART STILL
m <- lm(angleT ~ 0 + curvature1 + angle2 + angle1 + angle1:inPieceDistance2 + angle:inPieceDistance1, data = d)
summary(m)

#new try - THIS IS VERY GOOD, USE THIS! NOT ENOUGH DATA ON CURVATURE YET, NOTE! REMEMBER THE RANDOMNESS PART STILL
m <- lm(angleT ~ 0 + curvature:inPieceDistance1 + angle2 + angle1 + angle1:inPieceDistance2 + angle:inPieceDistance1, data = d)
summary(m)

#new try 2
m <- lm(angleT ~ 0 + curvature:inPieceDistance1 + angle2 + angle1 + angle:inPieceDistance1, data = d)
summary(m)


plot(predict(m,d) - d$angleT)
lines(d$angleT, col="red")
#new speed = a * prev throttle + b * current speed

par(mfcol=c(2,1))
i <- 1:nrow(d) #6000:8000
plot(predict(m,d[i,]),type="l", col="red", ylim=c(-10,10))
lines(d$angleT[i], col="green")
plot((predict(m,d) - d$angleT)[i], type="l")


m <- lm(angleT.res ~ 0 + factor(pieceIndex)*inPieceDistance1 + angle2, data = d)
summary(m)

par(mfcol=c(2,1))
i <- 6000:8000
plot(predict(m,d[i,]),type="l", col="red", ylim=c(-10,10))
lines(d$angleT.res[i], col="green")
plot((predict(m,d) - d$angleT.res)[i], type="l")




#spin out angle

plot(d$angle)
i <- which(d$angle[-1]==0 & d$angle[-n] != 0 & d$pieceIndex[-n] != 0)
sort(abs(d$angle[i]))

cbind(d$angle[i], d$angle[i-1], d$angle[i-2])

mean(sort(abs(d$angle[i])))
mean(sort(abs(d$angle[i-1])))
mean(sort(abs(d$angle[i-2])))

# -> spin out = angle > 60, keimola specific???


#switch lane
x0 = 96.5285910002
vdt = 9.70790281229
xdt = 4.23649381251
xds = 6.29676880544
vds = 9.76817780522

(x0 + vds - xdt)

sqrt(100^2 + 20^2)

100

20

>>> pS(bot.S[218:223])
v: 9.76577753982 x: 96.5285910002 p: 17 t: 1 a: 0.946296382197 w: 0.0400548754317 l: 1
v: 7.70790281229 x: 4.23649381251 p: 18 t: 1 a: 0.970794120141 w: 0.0244977379444 l: 1
v: 9.7705291983 x: 14.0070230108 p: 18 t: 1 a: 1.5833600615 w: 0.612565941358 l: 1
v: 9.77283271415 x: 23.779855725 p: 18 t: 1 a: 2.71869879385 w: 1.13533873235 l: 1
v: 9.77508932762 x: 33.5549450526 p: 18 t: 1 a: 4.31163488747 w: 1.59293609362 l: 1
>>> pS(bot.SS[218:223])
v: 9.76577753982 x: 96.5285910002 p: 17 t: 1 a: 0.946296382197 w: 0.0400548754316 l: 1
v: 9.76817780522 x: 6.29676880544 p: 18 t: 1 a: 0.970794120141 w: 0.0244977379444 l: 1
v: 7.75220391209 x: 11.9886977246 p: 18 t: 1 a: 0.98348860088 w: 0.012694480739 l: 1
v: 9.77283271415 x: 23.779855725 p: 18 t: 1 a: 2.71869879385 w: 1.13533873235 l: 1
v: 9.77508932762 x: 33.5549450526 p: 18 t: 1 a: 4.31163488747 w: 1.59293609362 l: 1

d <- rbind(read.csv("../python/messages-25.csv"))

track <- fromJSON(file="../python/keimola.json")

cc <- c(0.98099030155598621, 0.2046499698593508, 0.90000000000000702, -0.0012500000000000436, -0.29999999999999866, 0.29083529837885902, 0.5392914781255671)
which(d$startLaneIndex != d$endLaneIndex)

n = nrow(d)
d$vel[2:n] = d$inPieceDistance[2:n] - d$inPieceDistance[1:(n-1)]
d$nextVel = d$vel * cc[1] + d$throttle * cc[2]
d$nextPlace = d$inPieceDistance + d$nextVel
d$nextTruePlace = c(d$inPieceDistance[2:n],NA)

kv <- which(d$startLaneIndex != d$endLaneIndex & c(d$pieceIndex[-n] != d$pieceIndex[-1], FALSE))

kv.length <- length[d[kv,]$pieceIndex + 1]

kv.s <- which(!is.na(kv.length))
kv.c <- which(is.na(kv.length))



d.s <- d[kv[kv.s],]
d.s$pieceLength = kv.length[kv.s]

d.s$nextPlace-d.s$pieceLength-d.s$nextTruePlace

td <- 20

d.s$nextPlace-d.s$pieceLength-d.s$nextTruePlace

sqrt(d.s$pieceLength^2 + td^2) - d.s$pieceLength

(d.s$nextPlace-d.s$nextTruePlace) / sqrt(d.s$pieceLength^2 + td^2)

sqrt((d.s$pieceLength-td)^2 + td^2) - d.s$pieceLength + td

(td/d.s$pieceLength^2)

d.c <- d[kv[kv.c],]


length <- sapply(track$pieces, function(t) { ifelse(is.null(t$length), NA, t$length) })

radius <- sapply(track$pieces, function(t) { ifelse(is.null(t$radius), NA, t$radius) })
angle <- sapply(track$pieces, function(t) { ifelse(is.null(t$angle), NA, t$angle) })




kv.radius <- radius[d[kv,]$pieceIndex + 1]
kv.angle <- angle[d[kv,]$pieceIndex + 1]

d.c$pieceLength = abs(kv.radius[kv.c] * kv.angle[kv.c]/360*2*pi)
d.c$pieceLength1 = d.c$pieceLength + 10 * abs(kv.angle[kv.c]/360*2*pi)
d.c$pieceLength2 = d.c$pieceLength - 10 * abs(kv.angle[kv.c]/360*2*pi)

                     
d.c$nextPlace-d.c$pieceLength-d.c$nextTruePlace
d.c$nextPlace-d.c$pieceLength1-d.c$nextTruePlace
d.c$nextPlace-d.c$pieceLength2-d.c$nextTruePlace


d.c$truePieceLength = d.c$nextPlace-d.c$nextTruePlace

d.c$nextPlace-d.c$nextTruePlace

sqrt((d.c$pieceLength1^2 + d.c$pieceLength2^2)/2)

sqrt((d.c$pieceLength1^2 + d.c$pieceLength2^2)/2 + td^2)

d.c$truePieceLength / sqrt((d.c$pieceLength1^2 + d.c$pieceLength2^2)/2 + td^2)

d.c$truePieceLength / sqrt((d.c$pieceLength1^2 + d.c$pieceLength2^2)/2 + td^2)
d.c$angle

d.c$pieceLength
d.c$pieceLength1
d.c$pieceLength2

d.c$truePieceLength

cc <- c(0.9797520115844115, 0.19164729381060536, 0.89999999999998881, -0.001250000000000108, -0.29999999999996807, 0.28001727824967931, 0.52916658837239461)

ss <- c(6.85889308094, 57.7268582815, 6, 0, 54.4164515437, 1.32103316878, 1,
6.72001429329, 64.4468725748, 6, 0, 55.7052607136, 1.28880916982, 1,
6.58394752173, 0.344985390764, 7, 0, 56.9001631323, 1.19490241871, 1,
6.45063582858, 6.79562121934, 7, 1, 57.7192072133, 0.819044081028, 0,
6.51167072286, 13.3072919422, 7, 1, 57.7542451818, 0.246954482757, 0,
6.57146978331, 19.8787617255, 7, 1, 58.1429156066, 0.150458951493, 0,
6.63005803307, 26.5088197586, 7, 1, 58.5175550997, 0.105341467894, 0,
6.68745998863, 33.1962797472, 7, 1, 58.8881282927, 0.0893945630151, 0,
6.74369967006, 39.9399794173, 7, 1, 59.2414297803, 0.0810843329153, 0,
6.79880061108, 46.7387800284, 7, 1, 59.5450042186, 0.0611473324228, 0,
6.85278586887, 53.5915658972, 7, 1, 59.7377562819, 0.00706341997164, 0,
6.9056780338, 60.497243931, 7, 1, 59.7702026003, -0.0861309875407, 0,
6.95749923878, 67.4547431698, 7, 1, 59.564423628, -0.233965958133, 0,
7.0082711686, 74.4630143384, 7, 1, 59.0751380265, -0.428078445121, 0,
7.05801506898, 0.492969890663, 8, 1, 58.2304520341, -0.676801226164, 0)

s <- c(6.85889308094, 57.7268582815, 6, 0, 54.4164515437, 1.32103316878, 1,
6.72001429329, 64.4468725748, 6, 0, 55.7052607136, 1.28880916982, 1,
6.58394752173, 0.344985390764, 7, 0, 56.9001631323, 1.19490241871, 1,
6.45063582858, 6.79562121934, 7, 1, 57.5072906991, 0.60712756679, 0,
6.51167072286, 13.3072919422, 7, 1, 57.9924566551, 0.485165956065, 0,
6.57146978331, 19.8787617255, 7, 1, 58.4122136318, 0.419756976653, 0,
6.63005803307, 26.5088197586, 7, 1, 58.7987337297, 0.386520097942, 0,
6.68745998863, 33.1962797472, 7, 1, 59.1603454473, 0.36161171762, 0,
6.74369967006, 39.9399794173, 7, 1, 59.4838568862, 0.323511438838, 0,
6.79880061108, 46.7387800284, 7, 1, 59.7306928619, 0.246835975708, 0,
6.85278586887, 53.5915658972, 7, 1, 59.8563335879, 0.125640725965, 0,
6.9056780338, 60.497243931, 7, 1, 59.7983895861, -0.0579440017309, 0,
6.95749923878, 67.4547431698, 7, 1, 59.5032164717, -0.295173114461, 0,
7.0082711686, 74.4630143384, 7, 1, 58.9072532603, -0.595963211384, 0,
7.05801506898, 0.491545265373, 8, 1, 57.9648215024, -0.942431757843, 0)

s <- matrix(s,15,7,byrow=TRUE)
ss <- matrix(ss,15,7,byrow=TRUE)

colnames(s) <- c("v", "x", "p", "t", "a", "w", "l")
colnames(ss) <- c("v", "x", "p", "t", "a", "w", "l")

s <- as.data.frame(s)
ss <- as.data.frame(ss)

cc <- c(0.9797520115844115, 0.19164729381060536, 0.89999999999998881, -0.001250000000000108, -0.29999999999996807, 0.28001727824967931, 0.52916658837239461)

cs <- c(0.09534625892455922, 0.10540925533894598)

s$pw <- cc[3] * s$w + cc[4] * s$a * s$v + pmax(0, cc[5] * s$v + cc[7] * s$v * s$v * cs[1])  
s$dw <- s$w - s$pw
s

s$solved.cs <- (s$a[2:16] - s$a - cc[3] * s$w - cc[4] * s$a * s$v - cc[5] * s$v) / cc[7] / s$v / s$v
solved.cs <- s$solved.cs

plot(1/solved.cs)

plot(solved.cs)

i <- 3:13
si <- s[i, ]
plot(si$x, si$solved.cs)
plot(si$x, sqrt(si$solved.cs))

m <- lm(solved.cs ~ x, data = si[2:11,])
m <- lm(solved.cs ~ x + I(x^2), data = si[2:11,])
m <- lm(solved.cs ~ x + I(x^2) + I(x^3), data = si[2:11,])
summary(m)

lines(si$x[-1], predict(m))


plot
